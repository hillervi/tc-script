import csv, math, glob

def execute(data, column):
        
    AU = []
    
    i = 0
    for row in data:
        
        if i <= 3:
            pass
        
        else:
            AU.append(data[i][column])
            
        i = i + 1
    
    # Remove NaNs 
    AU = [i for i in AU if str(i) != 'NaN']
    
    total = float(0)
    for i in AU:
        total = float(total) + float(i)
      
    averageAU = total/len(AU)
    
    return averageAU

path = 'C:/research/*.txt'   
files = glob.glob(path)

for file in files:
      
    try:
     
        data = list(csv.reader(open(file, 'rb'), delimiter='\t'))
      
		with open('?/output.csv', 'ab') as csvfile:
        
            fieldnames = ['filename','AU6','AU17','AU23','AU24','Anger','Contempt','Disgust','Fear','Joy','Sad','Surprise','Neutral']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

	        writer.writerow({'filename': str(file),'AU6': execute(data, 36),'AU17': execute(data, 34),'AU23': execute(data, 39),'AU24': execute(data, 40),'Anger': execute(data, 47),'Contempt': execute(data, 48),'Disgust': execute(data, 49),'Fear': execute(data, 50),'Joy': execute(data, 51),'Sad': execute(data, 52),'Surprise': execute(data, 53),'Neutral': execute(data, 54)})
    
    except IOError:
        pass
    
    except ZeroDivisionError:
        pass
    

